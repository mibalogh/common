package recorder.common.listener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;

public class KeyExitListener {
	
	private static final Logger LOGGER = Logger.getLogger(KeyExitListener.class);
    
    public static void addExitKeyListener(IControlledService service) {
    	LOGGER.info("Please enter q to exit the program.\n");
    	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    	new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					while(service.isRunning()) {

						if(reader.readLine().equals("q")) {
							service.setRunning(false);
							service.shutdown();
						}
						
					}
				} catch (IOException e) {
					LOGGER.error("Problem in reading the standard input.");
				}
				
			}
		}).start();
    }
}
