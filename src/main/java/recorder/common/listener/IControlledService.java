package recorder.common.listener;

public interface IControlledService {

	boolean isRunning();
	
	void setRunning(boolean isRunning);
	
	void shutdown();
}
