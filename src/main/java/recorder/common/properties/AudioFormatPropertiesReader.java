package recorder.common.properties;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class AudioFormatPropertiesReader {
	
	private static final Logger LOGGER = Logger.getLogger(AudioFormatPropertiesReader.class);

	private static Properties sInstance = null;

	private static Properties getInstance() {
		if (sInstance == null) {
			try {
				sInstance = new Properties();
				InputStream in = AudioFormatPropertiesReader.class.getResourceAsStream("/audioformat.properties");
				sInstance.load(in);
			} catch (FileNotFoundException e) {
				LOGGER.error("Could not found file with properites." + e.getMessage());
			} catch (IOException e) {
				LOGGER.error("Could not read the file with properites." + e.getMessage());
			}
		}
		return sInstance;
	}
	
	public static Short getEncodedAudioFormat(String format) {
		return Short.valueOf((String) getInstance().get(format));
	}
	
	public static String getKey(Short value) {
		return (String) getInstance().entrySet().stream().filter(entry -> entry.getValue().equals(String.valueOf(value))).findFirst().get().getKey();
	}
}
