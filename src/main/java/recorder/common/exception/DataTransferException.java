package recorder.common.exception;

public class DataTransferException extends Exception {
	
	private static final long serialVersionUID = -7883974601289021756L;

	public DataTransferException() {
		super();
	}
	
	public DataTransferException(String message) {
		super(message);
	}

}
